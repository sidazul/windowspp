// function User(id, email) {
//     this.id = id;
//     this.email = email;
// }

// User.prototype.sayHi = () => {

// };

// const u = new User();
// u.sayHi();

// CLASS è la chiave per definire una classe. Serve a sostituire la definizione dei sotruttori ES5
// (orientati alle funzioni) a favore di una sintassi universalmente riconosciuta dai linguaggi orientato 
// alla Programmazione ad Oggetti (OOP - Object Oriented Programming)
class User {

    constructor(id, email) {
        this.email = email;
        this.id = id;
    }

    // Invece di agire sul prototype, posso definire i metodi direttamente all'interno della classe:
    sayHi() {

    }
}

// Per estendere una sottoclasse, non agisco più sul prototype ma uso la chiave extends:
// Admin (la sottoclasse) prenderà tutte le proprietà ed i metodi della superclass User
class Admin extends User {
    constructor(id, email) {
        // con super invoco il constructor di User
        super(id, email);
    }

    showId() {
        alert(`Sono l'admin con id $(super.id)`);
    //  SALUTA è un metodo inutile, anche Admin ha il metodo sayHi. L'ho inserito solo per farvi 
    //      vedere come accedere alle proprietà ed ai metodi della superclasse
    //     saluta() {
    //     // Con "super" accedo a metodi e proprietà della superclasse
    //     // Sto invocando sayHi dichiarato nell superclasse User
    //     super.sayHi();
    //     alert("Ma sono un admin e faccio come mi pare")
    }
}
new Admin(1, "email@dominio.com")