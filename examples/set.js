// // SET è un oggetto istanza che serve a contenere collezione di informazioni
// // A differenza degli array, in cui gli elementi possano ripetersi, SET non 
// // consente la ripetizione del medesimo elemento all'interno della collezione
// //  [1, 2, 1, 2] => OK in un Array
// //  NON consentito in un set

const { check } = require("prettier");

// const s = new Set();
// //s.add(1); //NON posso ripetere l'elemento numerico 1
// s.add(1);
// s.add("1");

// // HAS verifica la presenza di un elemento nella collezione (restituisce true o false)
// s.has(1)

// // DELETE rimuove un elemento dalla collezione
// s.delete(1);

// // CLEAR rimuovr tutti gli elementi dalla collezione
// s.clear();




// METODI DI ANDREA
class MySet {
    #_values = [];

    constructor() {

    }

    add(value) {
        // L'elemento contenuto in "value" esiste già
        // Non devo aggiungerlo nuovamente
        if (this.#_values.includes(value)) {
            return;
        }

        // L'elemento non è stato individuato nella collezione
        // quindi la aggiungo
        this.#_values.push(value);
    }

    clear() {
        // Elimino tutti gli elementi dall'array semplicemente creandone uno nuovo
        this.#_values = [];
    }

    delete(value) {
        // Individuo l'elemento "value" all'interno dell'array
        // indexOf restituisce un numero >= 0 se viene trovato un elemento nell'array, altrimenti -1
        const pos = this.#_values.indexOf(value);

        // Nel in cui l'elemento non sia stato individuato, non procedo
        if (pos === -1) {
            return;
        }

        // Elemento trovato, la rimuovo
        this.#_values.splice(pos, 1);
    }

    has(value) {
        // Restituisce il valore datomi da "includes": se value esiste, resistuirò true altrimenti false
        return this.#_values.includes(value);
    }
}

const ms = new MySet();


// METODI DI ARIANNA
// class Set {
//     constructor(value) {
//         this.value = value;
//     }

//     getSet() {
//         const newArray = [];

//         this.value.forEach ((checkValue) => {
//             if(newArray.indexOf(checkValue) == -1) {
//                 newArray.push(checkValue);
//             }
//         });

//         return newArray;
//     }
// }

// const doSet1 = new Set ([1, 2, 3, 4]);

// const doSet2 = new Set([1, 2, 3, 1]);

// console.log(doSet1.getSet());
// console.log(doSet2getSet());