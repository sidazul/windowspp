// MAP, a differenza degli oggetti Javascript (istanziati con {}) permette di creare
// delle coppie di dati (chiave - valore) dove la chiave può essere qualsiasi tipo
// di data
//
// Ad es:.
//    Se in un oggetto la chiave true viene convertita a stringa...
//      { true: "valore"} => true diventa "true" (la stringa true)
//    Con Map il valore true (booleano) rimarrà inalterato
//      Map() -> true => "valore"
//    per accedere a "valore" dovremo passare il valore booleano true e non la stringa "true"

// MAP è un oggetto istanza e come tale deve essere istanziato:
const m = new Map();
// SET permette di creare una associazione chiave-> valore
m.set(1, "Valore associato alla chiave 1 numerica")
m.set("1", "Valore associato alla chiave 1 stringa")
// GET permette di recuperare il valore associato ad una chiave
const val1number = m.get(1);
const val1string = m.get("1");

// CLEAR elimina tutte le coppie chiave -> valore
m.clear();

// DELETE rimuove una copia chiave -> valore a partire dalla chiave fornita
m.delete("1");

// KEYS permette di recuperare un array con tutte le chiavi presenti nel MAP
m.keys();

// Esempio di cache con Map
const cache = new Map();

function fnMoltoComplessa(v) {
    return v * 2;
}

// Una DECORATOR FUNCTION serve ad inglobare un'altra funzione e ad esterne le funzionalità
// Ad esempio, cacheDecorator prenderà una funzione e le avvolgerà intorna un meccanismo di cache
//
// I DECORATOR si applicano invocando il decorator, passandogli la funzione da "migliorare"
// e recuperando la nuova funzione che il decorator ci restituisce
function cacheDecorator(fn) {
    const cache = new Map();
    
    // La funzione restituita dal decorator sfrutta il Rest Operator per poter essere invocata
    // con un numero arbitraria di argomenti
    return v => {
        // HAS permette di stabilire se una chiave esiste all'interno di MAP
        // restituisce true / false
        if (cache.has(v)){
            return cache.get(v)
        }
        const res = fn(v);
        cache.set(v, res);
    
        return res
    }
}

// Applico il decorator della cache passando ad esso la funzione da "avvolgere"
const nuovaFnMoltoComplessa = cacheDecorator(fnMoltoComplessa);

// Per invocare la fnMoltoComplesso con il meccanismo di cache, uso la funzione
// che mi ha restituito il decorator
nuovaFnMoltoComplessa(4);


// 1 => "valore A"
// "1" => "valore B"
// true => "valore C"
// "true" => "valore D"


// MAP
// Permette di applicare una funzione a ciascun elemento dell'array
// I parametri della funzione sono identici a quelli della funzione che passo
// a forEach: elemento, indice (opzionale)
//
// Restituisco, nella funzione, il valore che dovrà trovarsi, alla posizione i nel nuovo array
var nuovoArray = a.map(function(e, i) {
    // Con return, nelle funzioni:
    // 1. Restituisco il valore
    // 2. Termino l'esecuzione
    return e * 2; 
});

// nuovoArray conterrà il risultato dell'applicazione di Map
// il vecchio array (a) rimarrà invariato
// a -> [1, 2, 3]
// nuovoArray -> [2, 4, 6];

// FILTER
// Mi permette di filtrare dei valori all'interno di un array
// Come Map, non modifica l'array di partenza ma ne crea uno nuovo
//
// Se la funzione che passo a Filter restituisce true, l'elemento su cui sto iterando
// viene aggiunto, altrimenti no
var arrayFiltrato = a.filter(function(e) {
    // < restituisce true se l'elemento di sinistra è più piccolo di quello di destra
    // Prima iterazione:
    //   e === 1 -> e è più piccolo di 3 quindi lo inserirò nel nuovo array
    // Seconda iterazione:
    //   e === 2 -> e è più piccolo di 3 quindi lo inserirò nel nuovo array
    // Terza iterazione:
    //   e === 3 -> e NON è più piccolo di 3 quindi NON lo inserirò nel nuovo array
    return e < 3;
});

//arrayFiltrato -> [1, 2];
