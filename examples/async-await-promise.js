const { resolveConfig } = require("prettier");

fetch()
    .then(response => response.json())
    .then(val => console.log(val))

// Una funzione asincrona inizia la sua firma con la chiave "ASYNC"
// All'interno delle funzioni asincrone posso utilizzare la parola chiave "await"
async function myAsyncFunc() {
    // AWAIT serve resgistrarsi in ascolta in ascolto ad una Promise
    const response = await fetch();
    const val = await response.json();
}

// è identico a:
// new Promise((resolve, reject) => {
//     fetch()
//         .then(response => response.json())
//         .then(val => resolve(val));
// })
//
// Il corpo della Promise (da { a }) equivale al corpo della funzione myAsyncFunc
// 
// Le funzioni asincrone sono trattate come Promise, tanto che posso agganciarmici
// con "then". La chiave "async" è quindi un wrapper di funzioni all'interno di
// Promise
// myAsyncFunc().then()


// Il concetto di PROMISE in javascript è simile a quello di callback. 
// E' come se nel programma che stiamo compilando inserissimo un segnaposto 
// con la promessa di occuparlo il prima possibile con un dato ed in cambio 
// il programma ci dia la possibilità di proseguire con le righe successive.
const p = new Promise(function(resolve, reject) {
    // Quello che te pare 
    // Resolve è una funzione che perde in
    // l'elemento da restituire in caso di successo
    return resolve(123);

    // Reject è una funzione che prende in ingresso
    //l'errore da restituire in caso di errore
    return reject();
});

p.then(
    function(success) {
        console.log(success);
    }, function(error) {
        console.error(error);
    }
);