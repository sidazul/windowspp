// CASTING
//   Un processo di conversione da un tipo di dato ad un altro
//   Può essere:
//     - esplicito -> sono io ad imporlo
//     - implicito -> quando avviene per altre cause (dipendenti dalla volontà dell'interprete)

// Converto la stringa "10" in un numero
// Esempio di CASTING ESPLICITO
var n = Number("10");
n = +"10" // Operatore +  in forma "unaria" (ha un solo operando)

// CASTING IMPLICITO
// L'operando numerico 10 (quello di sinistra) viene convertito in una stringa
// e concatenato all'operando di destra
var s = 10 + "10";

// Quando proviamo a convertire una stringa in un numero ma questa risulta
// incomprensibile, riceviamo il valore NaN (Not a Number)
var nonNumerico = +"abc10def"; // NaN

// Per verificare se un valore non è un numero (ovvero, se abbiamo ricevuto NaN)
// NON si può fare un confronto con l'operatore == oppure ===
// Dobbiamo necessariamente usare la funzione isNaN(nonNumerico)
nonNumerico === NaN // Restituisce false
isNaN(nonNumerico) // true