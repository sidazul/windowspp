/**
 * Le funzioni sono uno dei mattoni fondamentali in Javascript. Esse non sono altro che procedure, insiemi di istruzioni che eseguono compiti o calcolano valori.
 * Affinché queste procedure possano essere definite come funzioni, esse devono prendere degli input e restituire degli output, ove ci sia relazione tra questi elementi.
 * PER USARE UNA FUNZIONE NON BASTA DICHIARARLA, BISOGNA ANCHE ESEGUIRLA!
 */

// Una dichiarazione di funzione consiste nella parola chiave "function", seguita da:
// - Il nome della funzione stessa.
// - La lista dei parametri, racchiusi in parentesi tonde() e separati da virgole (,).
// - Gli statements che definiscono la funzione, racchiusi in parentesi graffe{}, che costituiscono il corpo della funzione.
function square(number) {
    return number * number;
  }
/**
 * I parametri primitivi (come un numero) sono passati alle funzioni per valore; 
 * il valore viene passato alla funzione, ma se la funzione cambia il valore del parametro, questo cambiamento non si riflette globalmente o nella funzione chiamante.
 * (Una funzione che moltiplica un valore come 2 non lo modificherà permanentemente.)
 * Valori non primitivi, come array oppure oggetti, invece, sono soggetti a cambiamenti permanenti se modificati da funzioni.
 */
 function myFunc(theObject) {
    theObject.make = 'Toyota';
  }
  
  var mycar = {make: 'Honda', model: 'Accord', year: 1998};
  var x, y;
  
  x = mycar.make; // x gets the value "Honda"
  
  myFunc(mycar);
  y = mycar.make; // y gets the value "Toyota"
                  // (the make property was changed by the function);

/**
 * Definire una funzione NON la esegue. Chiamare la funzione permette invece di svolgere effettivamente le azioni specificate.
 * Le funzioni devono essere "in scope" quando vengono chiamate!
 * Lo scope di una funzione è la funzione in cui è dichiarata (O l'intero programma, se è dichiarata al top level). 
 */


 // Gli argomenti di una funzione non sono limitati a stringhe e numeri. Si possono passare interi oggetti ad una funzione.
 // Inoltre, una funzione può chiamare se stessa, dando vita a una funzione ricorsiva.
 function factorial(n) {
    if ((n === 0) || (n === 1))
      return 1;
    else
      return (n * factorial(n - 1));
  }

/**
 * Le variabili definite all'interno di una funzione non sono accessibili da nessuna parte al di fuori della funzione, perché la variabile è definita solo nell'ambito della funzione.
 * Tuttavia, una funzione può accedere a tutte le variabili e funzioni definite all'interno dello scope in cui è definita.
 * QUINDI:
 * - Una funzione definita nell'ambito globale può accedere a tutte le variabili definite nell'ambito globale.
 * - Una funzione definita all'interno di un'altra funzione può anche accedere a tutte le variabili definite nella sua funzione madre, e a qualsiasi altra variabile a cui la funzione madre ha accesso.
 */