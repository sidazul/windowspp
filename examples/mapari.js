// MAP Set, Clear, Get, Delete
class MyEntry {
    constructor(key, value) {
        this.key = key;
        this.value = value;
    }
}

class MyMap {
    constructor() {
        this.entryArray = [];
    }

    getEntry(key) {
        // return this.entryArray.filter(function(value, index) {
        //     return value.key === key;
        // });

        //le chiavi dovrebbero essere univoche, ma questo è un problema per la me del futuro

        const pos = this.entryArray.findIndex(e => e.key === key);
        return this.entryArray[pos] || null;
        // return this.entryArray.filter(value => value.key === key).map(e => e.value);
    }

    has(key) {
        return this.entryArray.indexOf(e => e.key === key) >= 0
    }
    setMap(key, value) {
        if (this.has(key)) {
            return;
        }

         this.entryArray.push(new MyEntry(key, value));
         return this.entryArray;
    }

    clearMap() {
        this.entryArray = [];
    }

    deleteMap(key) {
        var delElArray = this.entryArray.filter(value => value.key !== key);
        this.entryArray = delElArray;
        return this.entryArray;
    }

}

const mm = new MyMap();

console.log(mm.setMap(6, 'I have a stupid cat'));
console.log(mm.getEntry('7'));
console.log(mm.setMap('7', 'stupid, stupid cat'));
console.log(mm.setMap('8', 'actually very stupid'));
console.log(mm.getEntry(6));
console.log(mm.deleteMap('8'));


// class MyMap {
//     #_keys = new Set();
//     #_values = [];

//     constructor() {

//     }

//     set(key, value) {
//         if (this.#_keys.has(key)) {
//             return;
//         }

//         this.#_keys.add(key);
//         this.#_values.push(key);
//     }
// }