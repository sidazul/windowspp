const cacheDecorator = fn => {
    const cache = new Map();
    
    // La funzione restituita dal decorator sfrutta il Rest Operator per poter essere invocata
    // con un numero arbitrario di argomenti
    return (...params) => {
        let k = "";

        params.forEach((e, i) => k.concat(`${i}: ${e}`))

        //   JOIN crea una stringa contenente tutti gli elementi di un array, separati
        //   attraverso il separatore specificato
        //      Ad es.: const k = params.map(p => `${typeof(p)}:${p}`).join(",");
        // HAS permette di stabilire se una chiave esiste all'interno di MAP
        // restituisce true / false
        const k = params.map(p => `${typeof(p)}:${p}`).join(",");
        if (cache.has(k)){
            return cache.get(k);
        }
        const res = fn(...params);
        cache.set(k, res);
    
        return res;
    }
}

const funzioneLenta = (v1, v2) => {
    // Ci metto un sacco di tempo... avrei proprio bisogna di una cache
    return v1 * v2;
};

const funzioneDecorata = cacheDecorator(funzioneLenta)
// params = ["1", "10"]
// map = ["string:1", "string:10"]
// join "string:1", "string:10"
funzioneDecorata("1", "10"); // string: 1, string: 10 --> 1,10
funzioneDecorata(1, "10"); // number: 1, string: 10 --> 1,10
