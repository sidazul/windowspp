// Dichiarazione di funzione (FUNCTION DECLARATION)
// 1. La parola chiave function
// 2. Il nome della funzione
// 3. Tra parentesi tonde i parametri FORMALI ovvero il nome che voglio dare
//    alle informazioni che passerò alla funzione
//    
//    Il parametro formale "a" riceverà delle informazioni al momento dell'invocazione
//    della funzione nomeFunzione
function nomeFunzione(a) {

}

// Invocazione:
// nome della funzione seguito da parentesi tonde aperte e chiuse
// all'interno delle parentesi tonde possiamo (dobbiamo) erogare tante informazioni
// quanti sono i parametri formali descritti sulla firma della funzione (ovvero sulla sua dichiarazione)
//
// Il valore che diamo di volta in volta al parametro formale "a" prende il nome
// di parametro ATTUALE
nomeFunzione(10);