// const xhr = XMLHttpRequest();
// xhr.open('GET', 'http://jsonplaceholder.typicode.com/users');

// xhr.addEventListener('loadend', () => {
//     console.log('data received');
//     xhr.response; // JSON da decodificare con JSON.parse();
// });

// xhr.send();

// FETCH fa parte delle API di ES6
// A differenza di XHR, che lavora per eventi, è basato sulle Promise
//
// Di default il verbo usato per l'interrogazione è il GET
fetch('http://jsonplaceholder.typicode.com/users').then(
    res => {
        if (res.status !== 200) {
            console.error('Qualcosa è andata male male');
            return;
        }

        return res.json();
    }, // JSON.parse(res.body) Gestisce la maggior parte dei casi, compresi errori del backend (404, 500 etc...)
                       // res.json() restituisce una Promise che risolverà con il contenuto della risposta
                       // ottenuta con la richiesta
    err => console.error(err) // Non gestisce casi particolari di errore se non quelli
                              // strettamente legati ad una richiesta che non è partita al backend
).then(
    data => console.debug(data)
)

// Esempio richiesta POST
fetch('https://jsonplaceholder.typicode.com/posts', {
  // Il secondo argomento permette di dettagliare, tra le altre cose,
  // il body della richiesta per quei verbi HTTP che ne fanno uso, gli header etc...
  
  // Corpo della richiesta, contenente le informazioni necessarie al backend per creare,
  // in questo caso, un nuovo post. Il body deve essere in formato JSON, così come specificato anche n
  // negli headers
  body: JSON.stringify({
    body: 'Corpo del post',
    title: 'Titolo del post',
    userId: 1
  }),
  // Gli header dettagliano la struttura della richiesta. Qui viene eventualmente
  // aggiunto il token di autenticazione
  headers: {
      'Content-Type': 'application/json; charset=UTF-8'
  },
  method: 'POST' // Può essere GET (default), POST, PUT, DELETE, PATCH...
}).then(res => res.json())
.then(data => console.log(data));