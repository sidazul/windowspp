const a = [1, 2, 3, 4, 5, 6];

/**
 * Dato un array, posso effettuare la destrutturazione dichiarando una o più costanti / variabili
 * all'interno delle parentesi quadre
 * 
 * A destra dell'operatore di assegnazione è posta la reference all'array da destrutturare
 * 
 * const [costanti] = array da destrutturare
 * 
 * La destrutturazione negli array funziona per POSIZIONE, quindi la prima costante prenderà il
 * valore posto alla prima posizione dell'array (0), la seconda il valore alla seconda posizione (1)
 * e così via...
 * 
 * Posso saltare uno o più valori inserendo una virgola senza specificare il nome della costante:
 * 
 * const [u1, , u3] = a;
 */
const [u1, u2, u3] = a;

const o = {
    name: 'nome',
    surname: 'surname'
};

/**
 * La destrutturazione degli oggetti funziona, invece che per posizione, per chiave.
 * 
 * Si dichiarano una o più costanti il cui nome coinciderà con una chiave presente all'interno dell'oggetto
 * 
 * Nell'esempio sottostante, la costante "name" prenderà il valore associato alla chiave "name" nell'oggetto "o",
 * discorso analogo per la costante "surname" che si vedrà associare iSl valore presente nella chiave "surname"
 */
const { name, surname } = o;






