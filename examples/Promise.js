// LE PROMISE

// Sono uno strumento per schetulare le task.
// Andiamo a delegare l’esecuzione di alcuni task all’interno delle promise.
// Le promise sono degli oggetti che prendono in input una funzione. Passano 
// nel loro ciclo di vita per due stati (ma la documentazione ne da tre).


const p = new Promise((resolve, reject) => {
    return resolve("valore da restituire");
});

p.then (
    successo => console.log(successo),
    errore => console.error(errore)
);

fetch().then(
    response => {},
    error => console.error("timeout")
)