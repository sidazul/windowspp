// FOR-IN
const o = {
  k1: "valore",
  k2: 10,
  chiappa2: {},
};

// const key conterrà la chiave che di volta in volta viene
// analizzata dal for-in
for (const key in o) {
  // o.key => Accedo alla proprietà "key" nell'oggetto "o"
  // NON è corretta perchè la costante key non viene interpellata

  // Accedo alla chiave presente in "key" (la prima volta key conterrà
  // "k1", la seconda volta "k2", la terza volta "chiappa2"...)
  // all'interno dell'oggetto "o"
  console.log(key, o[key]);
}

// ES5
Object.keys(o).forEach(function (key) {
  console.log(key, o[key]);
});

// in (operator)
const o2 = {
  k1: undefined,
  alessandroBorghese: 10,
};

// Con questa sintassi non controllo se esiste la chiave
// bensì il valore ad essa associato

// if (o2.k1) {
//   console.log("k1 esiste");
// }

// if (o2.alessandroBorghese) {
//   console.log("alessandroBorghese esiste");
// }

// Anche una chiave che non esiste in un oggetto restituisce
// "undefined", come spesso accade in JavaScript quando si tenta
// di accedere a degli elementi inesistenti

// if (o2.chiaveCheNonEsiste) {

// }

// "In" mi permette di verificare se la chiave presente in "o2"
// esiste, indipendentemente dal valore associato ad essa
if ("k1" in o2) {
  console.log("k1 esiste");
}

if ("alessandroBorghese" in o2) {
  console.log("alessandroBorghese esiste");
}