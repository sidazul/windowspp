// WHILE

// È un costrutto di iterazione
// Si utilizza quando il numero di iterazioni NON è noto
//
// È un ciclo con controllo in TESTA
// while( condizione per cui continuare ad iterare )
//
// Se la condizione per cui iterare non è MAI verificata
// NON eseguirò quello che ho scritto nelle {} nemmeno una volta

while(false) {

}

// DO {} - WHILE ()
//
// È un costrutto di iterazione
// Si utilizza quando il numero di iterazioni NON è noto
//
// A differenza di While, viene eseguita ALMENO UNA ITERAZIONE perchè il controllo
// viene eseguito in CODA
//
// Esercizio:
// Prova a calcolare quante volte viene ripetuto il "do"
var n;

do {
    // Genera un numero casuale
    n = Math.random();
} while(n >= 0.5);

console.debug(n);