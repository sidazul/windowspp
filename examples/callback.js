// CALLBACK
// Una funzione che viene invocata da un'altra funzione

var a = [1, 2, 3];

function map(arr, funzioneDaApplicare) {
    var nuovoArray = [];

    for (var i = 0; i < arr.length; i++) {
        var risultato = funzioneDaApplicare(a[i]);
        nuovoArray.push(risultato);

        // Si può semplificare in:
        // nuovoArray.push( funzioneDaApplicare(a[i]) );
    }

    return nuovoArray;
}

var numeriDoppi = map(a, function(e) {
    return e * 2; 
});

console.debug(numeriDoppi);

