// Il tipo di dati booleano può essere uno di due valori, vero o falso. 
// I booleani sono usati per rappresentare i valori di verità associati 
// al ramo logico della matematica, che informa gli algoritmi nell'informatica.
const b1 = !false; // true
const b2 = true && false; // false
const b3 = false || true; // true
const b4 = 123 === "456"; // false
const b5 = 1.23 === 123e-2; // true





// I datatype primitivi :(che sono allocati nella zona di memoria della RAM)
// -numerici: la categoria è detta “number”;agglomera n° interi e reali
// -caratteri alfanumerici: detti “string”. Contiene sia caratteri singoli che sequenze di caratteri;
// -booleani: detti “boolean”. I valori sono true/false;
// -nulli: detti “null”. Ammette un solo valore: null;
// -indefiniti: detti “undefined”. Ammette un solo valore: undefined.

// Datatype complessi : (che vengono collocati nella zona di memoria dell’HEAP)
// -Array: sono sequenze ordinate di informazioni. Ad esempio un insieme di numeri.
// -Object: Ogni scatola con un valore, agglomerato di chiavi -> valori.