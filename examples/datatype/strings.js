// STRING
//   - Data tipe primitivo
//   - Sequenze di caratteri
//   - Dichiarate con '' oppure con ""
var s = 'contenuto della stringa';

var nuovaStringa = s.concat(' ed il suo seguito'); // contenuto della stringa ed il suo seguito

// Serve a prelevare un pezzetto di una stringa
// È un metodo che prende in input almeno un parametro
//   1. Indice da cui iniziare a tagliare la stringa
//   2. Indice in cui terminare il taglio (in assenza del parametro, si arriva alla fine della stringa)
s.substring(9); // della stringa

s.startsWith('contenuto'); // true
s.startsWith('della') // false

// Concatenazione di stringhe con operatore +
"ciao" + " Silvia"; // Le due stringhe si uniscono in "ciao Silvia"
// Fare attenzione ad alcuni casi particolari
"ciao" + 10; // "ciao10"
10 + "ciao" // "10ciao"