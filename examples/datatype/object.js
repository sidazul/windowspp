// OBJECT
// Sono, come gli array, degli agglomerati di informazioni
//
// Invece di indivudare le informazioni attraverso un indice numerico,
// sfruttano un nome (chiave): gli oggetti sono coppie di chiavi e valori
//
// Si creano con le parentesi graffe {} (object literal)
var o = {
    // All'interno di una chiave posso dichiarare una funzione:
    // questa sarà identificata come "metodo"
    calcolaIlDoppio: function() {
        // uso this perchè altrimenti cercherei nel metodo una variabile con il nome
        // "nomeChiave". Voglio invece leggere il contenuto dell'oggetto alla chiave
        // "nomeChiave", quindi uso "this"
        //
        // "this" è un puntatore alla zona di memoria in cui vive l'oggetto
        return this.nomeChiave;
    },
    // nome della chiave: valore
    nomeChiave: 10
};

console.debug(o.nomeChiave);
console.debug(o.calcolaIlDoppio());

// Così copio il valore dello stack (il puntatore allo HEAP)
// quindi entrambe le variabili puntano alla medesima zona di memoria
// NON ho clonato l'oggetto: modificando la chiave in una delle due variabili
// altero il valore anche nell'altra
var nonUnaCopia = o;
nonUnaCopia.nomeChiave = 20;
console.debug(nonUnaCopia.nomeChiave, o.nomeChiave);

// OBJECT ASSIGN
// Copiare le chiavi ed i valori all'interno di un oggetto
// Se l'oggetto di destinazione è un nuovo oggetto, creo un nuovo spazio nello HEAP
// il secondo argomento da passare sarà l'oggetto di cui effetuare la copia
var unaCopiaVera = Object.assign({}, o);
// Non altero l'oggetto "o" perchè ho creato un nuovo elemento in memoria
unaCopiaVera.nomeChiave = 100;


