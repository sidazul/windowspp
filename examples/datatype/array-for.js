// ARRAY
//
// Sequenze di informazioni
// Ciascuna informazione della sequenza è individuata attraverso un indice numerico
// che parte da 0
//
// Gli array si creano con le parentesi QUADRE;
var a = [1, 2, 3];

// Inserire valori in coda (alla fine dell'array)
a.push(4);

// Inserire i valori in testa (all'inizio dell'array)
a.unshift(0);

// Prelevare un elemento dall'array
var ultimo = a.pop(); // Restiuisce l'elemento che si trova in CODA e lo elimina

// Prelevare il primo elemento dall'array
var primo = a.shift(); // Anche shift rimuove l'elemento che viene restiuito (come pop)

// Come accedere agli elementi di un array senza eliminarli fisicamente
// All'interno delle parentesi quadre indico l'indice rispetto al quale prelevare l'informazione
a[0];

// Come leggere tutti gli elementi di un array
//
// Con il ciclo FOR
// Si applica quando il numero iterazioni è noto (coincide con la lunghezza dell'array)

// 1. Diciarazione dell'indice e sua inizializzazione (solitamente partiamo dalla prima
//    posizione, ovvero l'indice 0)
// 2. Condizione per cui continuare ad iterare (nell'analisi di un array è i < lunghezza array)
//    a.length restituisce il numero di elementi dell'array
//    se a contiene [1, 2, 3] la proprietà length mi restituisce 3
//    anche se le posizioni degli indici sono 0, 1, 2
// 3. Incrementiamo l'indice alla fine di ciascuna iterazione
//    i++ è uguale a i = i + 1
for (var i = 0; i < a.length; i++) {
    // Visto che i contiene l'indice da cui prelevare l'informazione nell'array
    // La prima volta che itererò i varrà 0, quindi leggerò il primo elemento
    // La seconda volta varrà 1, quindi leggerò il secondo
    // etc etc...
    console.log(a[i]);
}

// Come il FOR, mi permette di iterare su tutti gli element di un array
// Prende una funzione da eseguire ad ogni iterazione
// Praticamente il corpo della funzione che passo coincide con quello che potrei
// meterre nelle graffe di un ciclo for
a.forEach(function(e, i) {
    // La funzione che passo a forEach è ANONIMA perchè non ha un nome
    //
    // Il parametro formale "e" conterrà, di volta in volta, l'elemento dell'array
    // su cui sto iterando
    //
    // Non devo preoccuparmi di gestire la "e" perchè è un valore che viene dato direttamente
    // dall'interprete
    console.log(e);

    // È comodo perchè mi asula dalla gestione dell'indice
});






