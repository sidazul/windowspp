// HOISTING

// L’ ECMA utilizza un processo di FETCH (fase) , ovvero, fa un analisi del 
// codice, poi adatta l'hosting, appuntando il var (delle variabili) da una parte, 
// lasciando solo l’assegnazione , così che riesce a interpretare  anche il codice 
// scritto male che restituisce undefined.
//
// Grazie al processo (meccanismo) di Hoisting verranno comunque entrambe lette e risolte 
// dell’ECMA. La seconda però , mi restituirà “undefined” dato che non ho inizializzato 
// ( quando assegno un valore a una variabile) la variabile. Perciò possiamo dire che il 
// risultato “undefined” è una variabile esistente, ma mai inizializzata.
// Oppure quando un “calcolo o una funzione” non ci restituiscono un valore troviamo undefined.


console.log(x); // stamperà undefined perchè la variabile non ha ancora ricevuto il valore 10

// var è soggetto ad hoisting
// La dichiarazione var x viene portata in cima allo script (prima della riga 1)
// viene assegnato all'identificatore il valore undefined
var x = 10;

nomeFunzione();

// Anche le funzioni sono soggette ad hoisting
// posso invocare una funzione anche prima della sua dichiarazione
function nomeFunzione() {

}

// DOM
// - Il crooling del DOM, come con JS andare ad analizzare la struttura del DOM senza vedere necessariamente gli id.

// - XML (linguaggio), è molto simile a HTML, è diviso in rami e fogli.
// Quindi la foglia è figlia del ramo.
