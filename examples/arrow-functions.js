var fn = function() {

}

// Si chiamano ARROW FUNCTION poichè la simbologia =>
// ricorda una freccia
//
// => separa la parte dichiarativa (la firma) dal corpo
//
const f = () => {};

// Quando ho 1 solo parametro formale, posso omettere
// le parentesi tonde. Ad es:
//    a => {}
//
// Non è necessario quindi scrivere:
//   (a) => {}

// Quando ho più di un parametro formale, sono costretto
// a reintrodurre le parentesi tonde. Ad es:
//   (a, b) => {}

// Quando il corpo della funzione è costituito
// da UNA SOLA ISTRUZIONE, le parentesi graffe possono
// essere omesse. Ad es:
//   n => n * 2
//
// Quando non sono presenti le parentesi graffe,
// Il risultato della singola espressione viene
// restituito automaticamente (come se prima di questa
// avessimo scritto "return")
//
// Attenzione: in presenza delle parentesi graffe
// il return deve essere esplicitato come sempre




//ES5:
// function fnName( ){

// } //function DeclaratiOn

// //function expression (anonima)
// var fn = function(){ 

// }


//________________________________________________________________________
//ES6
//Le ARROW function si chiamano così perchè ricordano una freccia --> "=>"
//Le ARROW function sono funzioni anonime in ES6, ma differiscono
//nella sintassi, non c'è la keyword function
//Se non ho parametri metto le ()
//La  " =>" stacca la parte dichiarativa dal corpo
//const f = () => {};

//Quando ho un solo parametro formale posso omettere le parentesi ()
//Ad esempio: a => {}
//Non è quindi necessario scrivere (a) => {}
//
//Quando ho più di un parametro formale sono costretto a
//reitrodurre le parentesi ()
//Ad esempio : (a,b) => { corpo della funzione/istruzione }
//
//Quando il corpo della funzione è costituito da una sola istruzione
//le { } possono essere omesse
//Ad esempio: n => n * 2
//Il risultato ha un "return" implicito. Però questo solo quando
//c'è una sola istruzione, quindi quando le {} sono omesse.

// !! ATTENZIONE !! : In presenza delle { } il return va messo!!!


