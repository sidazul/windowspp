module.exports = {
    devtool: 'source-map',
    entry: {
        chart: './src/chart.js',
        comment: './src/comment.js',
        hamburger: './src/hamburger.js',
        main: './src/app.js',
        meteo: './src/meteo.js',
        post: './src/post.js',
        prenotazione: './src/prenotazione.js',
        socket: './src/socket.js',
        'try-catch': './src/try-catch.js',
        users: './src/users.js'
    },
    mode: 'development',
    module: {
        rules: [{
            exclude: /node_modules/,
            use: [{
                loader: 'babel-loader'
            }, {
                loader: 'prettier-loader'
            }],
            test: /\.js$/
        }]
    },
    output: {
        filename: '[name].bundle.js',
        path: __dirname + '/dist'
    }
}