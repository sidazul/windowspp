import Chart from "chart.js/auto";

// Prelevo il contesto del canvas nel quale poter disegnare
const ctx = document.getElementById("chart").getContext("2d");

// Chart data
// Due Arr vuoti: sono i numeri di causali generati da Math.random
const data = [];
const labels = [];

const chart = new Chart(ctx, {
  type: "line",
  data: {
    labels,
    datasets: [
      {
        label: "My First Dataset",
        data,
        fill: false,
        borderColor: "rgb(75, 192, 192)",
        tension: 0.1,
      },
    ],
  },
  options: {
    scales: {
      y: {
        beginAtZero: true,
      },
    },
  },
});

// è il punto di partenza(x), incrementato in un ogni secondo
let lastYear = 1960;

//Il metodo setInterval() chiama una funzione o valuta
//un'espressione a intervalli specificati
setInterval(() => {
  //verrà aggiunto un numero causale nella const data = [].
  data.push(+(Math.random() * 100).toFixed());
  // Se utilizzato come postfisso, con l'operatore dopo l'operando x++,
  // l'operatore di incremento, incrementa e restituisce il valore prima dell'incremento.
  // Se utilizzato come prefisso, con l'operatore prima dell'operando ++x,
  // l'operatore di incremento, incrementa e restituisce il valore dopo l'incremento.
  //
  // Il numero di lastYear verrà incrementato di 1.
  //
  // Verrà aggiunto un numero causale nella const labels = [].
  labels.push(++lastYear);
  // aggiornamento del grafico
  chart.update();
}, 1000);

// import Chart from "chart.js/auto";

// function initChart() {
//   return new Chart(ctx, {
//     type: "line",
//     data: {
//       labels,
//       datasets: [
//         {
//           label: "My First Dataset",
//           data,
//           fill: false,
//           borderColor: "rgb(75, 192, 192)",
//           tension: 0.1,
//         },
//       ],
//     },
//     options: {
//       animation: false,
//       scales: {
//         x: {
//           beginAtZero: true,
//           max: 140,
//         },
//         y: {
//           beginAtZero: true,
//           max: 100,
//         },
//       },
//     },
//   });
// }

// // Prelevo il contesto del canvas nel quale poter disegnare
// const ctx = document.getElementById("chart").getContext("2d");

// // Chart data
// let chart = initChart();
// let data = [];
// let i = 1;
// let labels = [];

// setInterval(() => {
//   // Parte iniziale e finale del grafico
//   if (i <= 20 || i >= 120) {
//     data.push(50);
//   } else if (i > 45 && i <= 95) {
//     // else if: discesa
//     data.push(data[data.length - 1] - 1);
//   } else {
//     // salita
//     data.push(data[data.length - 1] + 1);
//   }

//   labels.push(i);
//   chart.update();

//   if (i === 140) {
//     console.log(data);
//     i = 0;
//     data = [];
//     labels = [];
//     chart.destroy();
//     chart = initChart();
//   }

//   i++;
// }, 100);
