import { Weather } from "./classes/weather";

const city = document.getElementById("city");
const forecast = document.getElementById("forecast");
const icon = document.getElementById("icon");
const temp = document.getElementById("temp");

// Con Promise chaining:
// weather.update().then(data => console.log(data));

// Per poter utilizzare async / await dobbiamo necessariamente trovarci dentro una funzione
// che sia dichiarata come async
const load = async () => {
  const weather = new Weather("Rome", "IT-62", "IT");
  const data = await weather.update();

  city.innerText = `${data.city.name} - ${data.description}`;

  forecast.innerHTML = `
    ${data.temperature.current}°C (${data.temperature.feelsLike} percepiti)
    Min: ${data.temperature.min} - Max: ${data.temperature.max}
  `;

  icon.src = data.iconUrl;

  console.log(data);

  // Utilizzando il getter di una classe, accedo al valore come se leggessi il contenuto di una proprietà:
  // i.e. leggo il valore del getter iconUrl
  // forecast.iconUrl

  // Invocando un metodo, uso invece le parentesi tonde:
  // i.e. forecast.getIconUrl()
};

load();
