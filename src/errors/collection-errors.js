// Creo una classe di errore personalizzata che estende
// quella "base" di errore
export class IndexRelatedError extends Error {
    constructor(message) {
      super(message);
    }
  }
  