const sck = new WebSocket(
  "wss://demo.piesocket.com/v3/channel_1?api_key=oCdCMcMPQpbvNjUIzqtvF1d2X2okWpDQj4AwARJuAgtjhzKxVEjQU6IdCjwm&notify_self"
);

sck.addEventListener("message", ev => {
  console.log(ev.data);
});

const form = document.querySelector("form[name='comment']");
const message = document.querySelector("input[name='message']");
const id = document.getElementById("message");

form.addEventListener("submit", e => {
  e.preventDefault();

  sck.send(message.value);
});
