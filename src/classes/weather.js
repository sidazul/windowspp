import { config } from "../../environments/develop";

// Models
import { City } from "../models/city";
import { Forecast } from "../models/forecast";
import { Temperature } from "../models/temperature";
import { Wind } from "../models/wind";

export class Weather {
  constructor(city, state, country) {
    this.city = city;
    this.country = country;
    this.state = state;
  }

  // update() {
  //     fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.state},${this.country}&appid=${config.weather.apiKey}`)
  //         .then(response => response.json())
  //         .then(data => {
  //             // Trasformo i dati in un Model "Forecast"
  //         });
  // }

  /**
   * Fetch the weather data from the OpenWeatherMap API
   *
   * @param {string} lang Language of the response text
   * @returns {Promise}
   */
  async update(lang = "it") {
    const response = await fetch(
      `http://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.state},${this.country}&appid=${config.weather.apiKey}&units=metric&lang=${lang}`
    );
    const data = await response.json();

    return new Forecast(
      new City(
        data.name,
        data.sys.country,
        data.coord.lat,
        data.coord.lon,
        data.timezone,
        data.sunrise,
        data.sunset
      ),
      data.weather[0].description,
      new Temperature(
        data.main.temp,
        data.main.temp_min,
        data.main.temp_max,
        data.main.feels_like
      ),
      new Wind(data.wind.speed, data.wind.deg, data.wind.gust),
      data.main.pressure,
      data.main.humidity
    );
  }
}

const w = new Weather("rome", "IT-62", "IT");
w.update();
