import { Post } from "../models/post";

/**
 * Read a post for the given id
 *
 * @param {number} id The post id
 * @returns {Promise<Post>}
 */
export const getPost = async id => {
  // return fetch(`http://jsonplaceholder.typicode.com/posts/${id}`)
  //     .then(response => response.json())
  //     .then(data => new Post(data.id, data.title, data.body, data.userId));

  const response = await fetch(
    `http://jsonplaceholder.typicode.com/posts/${id}`
  );
  const data = await response.json();

  return new Post(data.id, data.title, data.body, data.userId);
};
