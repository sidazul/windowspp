// Esempio di login con callback per la gestione dell'avvenuto
// accesso

// export const login = (email, password, cb) => {
//   fetch("https://reqres.in/api/login", {
//     body: JSON.stringify({
//       email,
//       password,
//     }),
//     headers: {
//       "Content-Type": "application/json; charset=UTF-8",
//     },
//     method: "POST",
//   }).then(res => res.json())
//   .then(data => cb(data));
// };

// Restituendo la Promise che trasforma il JSON in una struttura dati
// quale array od oggetto, posso invocare il metodo login in qualsiasi
// punto del mio script ed agganciarmi alla risposta con il metodo then
//
// Es. vedi file src/users.js
export const login = (email, password) => {
  return fetch("https://reqres.in/api/login", {
    body: JSON.stringify({
      email,
      password,
    }),
    headers: {
      "Content-Type": "application/json; charset=utf-8",
    },
    method: "POST",
  }).then(res => res.json());
};
