import { Collection } from "./classes/collection";

let attempts = 1;
let socket;

const connect = url => {
  let hasError = false;

  try {
    // Blocco di codice che, potenziamente, potrebbe generare un errore
    // Gli errori vevngono anche chiamati "eccezioni"
    socket = new WebSocket(url);
  } catch (error) {
    // All'interno del catch individuiamo il blocco di codice
    // da eseguire qualora si verifichi un errore nelle istruzioni contenute
    // nel blocco "try"
    //
    // L'errore viene intercettato, inserito nella variabile "errore"
    // e non risulterà bloccante per l'applicativo
    alert("Connessiona al socket fallita");
    attempts++;
    hasError = true;
  } finally {
    // Un blocco di codice che eseguiamo sia al verificarsi di un errore, sia
    // qualora il codice contenuto in "try" non generi eccezioni
    //
    // In caso di errore: try -> catch -> finally
    // In assenza di errore: try -> finally
    if (attempts <= 3 && hasError) {
      connect(url);
      return;
    }
  }
};

// Esempio con la classe COLLECTION
const c = new Collection();
c.add("a"); // 0
c.add("b"); // 1
c.add("c"); // 2

// Un errore non gestito stampa in console "Uncaught Error"
// c.get(30); // L'indice 30 non esiste

try {
  c.get(30);
} catch (error) {
  alert("L'indice selezionato non esiste");
}
