export class Post {
  constructor(id, title, body, userId) {
    this.body = body;
    this.id = id;
    this.title = title;
    this.userId = userId;
  }
}
