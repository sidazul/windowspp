export class Comment {
  constructor(id, email, name, body, postId) {
    this.body = body;
    this.email = email;
    this.id = id;
    this.name = name;
    this.postId = postId;
  }
}
