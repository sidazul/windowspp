export class Temperature {
  constructor(current, min, max, feelsLike) {
    this.current = current;
    this.feelsLike = feelsLike;
    this.max = max;
    this.min = min;
  }
}
