export class Forecast {
  /**
   * @param {City} city
   * @param {string} description
   * @param {Temperature} temperature
   * @param {Wind} wind
   * @param {number} pressure
   * @param {number} humidity
   */
  constructor(city, description, temperature, wind, pressure, humidity) {
    this.city = city;
    this.description = description;
    this.humidity = humidity;
    this.pressure = pressure;
    this.temperature = temperature;
    this.wind = wind;
  }
  // Un getter (un metodo che usa come parola chiave "get") crea una proprietà fittizia all'interno dell'istanza
  // il cui valore è ciò che viene restituito dal metodo
  // i.e. this.iconUrl <- NO parentesi tonde (si comporta come una proprietà)
  get iconUrl() {
    return `http://openweathermap.org/img/wn/${this.icon}@2x.png`;
  }

  // Questo è un metodo qualsiasi, e per leggerne il valore restituito devo invocarlo
  // i.e. this.getIconUrl() <- SI parentesi tonde (è un metodo, deve essere invocato)
  getIconURL() {
    return `http://openweathermap.org/img/wn/${this.icon}@2x.png`;
  }
}
