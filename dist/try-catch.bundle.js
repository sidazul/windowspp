/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/classes/collection.js":
/*!***********************************!*\
  !*** ./src/classes/collection.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Collection": () => (/* binding */ Collection)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classPrivateFieldInitSpec(obj, privateMap, value) { _checkPrivateRedeclaration(obj, privateMap); privateMap.set(obj, value); }

function _checkPrivateRedeclaration(obj, privateCollection) { if (privateCollection.has(obj)) { throw new TypeError("Cannot initialize the same private elements twice on an object"); } }

function _classPrivateFieldGet(receiver, privateMap) { var descriptor = _classExtractFieldDescriptor(receiver, privateMap, "get"); return _classApplyDescriptorGet(receiver, descriptor); }

function _classExtractFieldDescriptor(receiver, privateMap, action) { if (!privateMap.has(receiver)) { throw new TypeError("attempted to " + action + " private field on non-instance"); } return privateMap.get(receiver); }

function _classApplyDescriptorGet(receiver, descriptor) { if (descriptor.get) { return descriptor.get.call(receiver); } return descriptor.value; }

var _elements = /*#__PURE__*/new WeakMap();

var Collection = /*#__PURE__*/function () {
  function Collection() {
    _classCallCheck(this, Collection);

    _classPrivateFieldInitSpec(this, _elements, {
      writable: true,
      value: []
    });
  }

  _createClass(Collection, [{
    key: "add",
    value: function add(e) {
      _classPrivateFieldGet(this, _elements).push;
    }
  }, {
    key: "get",
    value: function get(i) {
      var e = _classPrivateFieldGet(this, _elements)[i];

      if (e) {
        return e;
      } // Gli errori  (eccezioni) si sollevano (raise) o lanciano (throw)
      // l'istruzione da usare è di solito throw, e nel gergo informatico
      // si dice che "to rise an exception"
      //
      // Gli errori sono oggetti istanza Errore
      // throw new Error('Element not found for the given id');
      //
      // Quando si scatena l'errore?
      //   - get prende un indice
      //   - l'indice viene usato per prelevare un elemento dall'array #_elements
      //   - se l'elemento non esiste (non c'è un elemento alla posizione i) lancio un errore


      throw new IndexRelatedError("Element not found for the given index");
    }
  }]);

  return Collection;
}();

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**************************!*\
  !*** ./src/try-catch.js ***!
  \**************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classes_collection__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/collection */ "./src/classes/collection.js");

var attempts = 1;
var socket;

var connect = function connect(url) {
  var hasError = false;

  try {
    // Blocco di codice che, potenziamente, potrebbe generare un errore
    // Gli errori vevngono anche chiamati "eccezioni"
    socket = new WebSocket(url);
  } catch (error) {
    // All'interno del catch individuiamo il blocco di codice
    // da eseguire qualora si verifichi un errore nelle istruzioni contenute
    // nel blocco "try"
    //
    // L'errore viene intercettato, inserito nella variabile "errore"
    // e non risulterà bloccante per l'applicativo
    alert("Connessiona al socket fallita");
    attempts++;
    hasError = true;
  } finally {
    // Un blocco di codice che eseguiamo sia al verificarsi di un errore, sia
    // qualora il codice contenuto in "try" non generi eccezioni
    //
    // In caso di errore: try -> catch -> finally
    // In assenza di errore: try -> finally
    if (attempts <= 3 && hasError) {
      connect(url);
      return;
    }
  }
}; // Esempio con la classe COLLECTION


var c = new _classes_collection__WEBPACK_IMPORTED_MODULE_0__.Collection();
c.add("a"); // 0

c.add("b"); // 1

c.add("c"); // 2
// Un errore non gestito stampa in console "Uncaught Error"
// c.get(30); // L'indice 30 non esiste

try {
  c.get(30);
} catch (error) {
  alert("L'indice selezionato non esiste");
}
})();

/******/ })()
;
//# sourceMappingURL=try-catch.bundle.js.map