/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/functions/comments.js":
/*!***********************************!*\
  !*** ./src/functions/comments.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "addComment": () => (/* binding */ addComment),
/* harmony export */   "deleteComment": () => (/* binding */ deleteComment),
/* harmony export */   "getComment": () => (/* binding */ getComment),
/* harmony export */   "getComments": () => (/* binding */ getComments),
/* harmony export */   "putComment": () => (/* binding */ putComment)
/* harmony export */ });
/* harmony import */ var _models_comment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../models/comment */ "./src/models/comment.js");
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


/**
 * Create a comment accordingly to the given params
 *
 * @param {string} email User's email
 * @param {string} name User's name
 * @param {string} body Content of the comment
 * @param {number} postId Id of the post
 * @returns {Promise<Comment | null>}
 */

var addComment = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(email, name, body, postId) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/posts/".concat(postId, "/comments"), {
              body: JSON.stringify({
                // È una contrazione per body: body, email: email etc etc
                body: body,
                email: email,
                name: name,
                postId: postId
              }),
              headers: {
                "Content-Type": "application/json; charset=utf-8"
              },
              method: "POST"
            });

          case 2:
            response = _context.sent;
            _context.next = 5;
            return response.json();

          case 5:
            data = _context.sent;
            return _context.abrupt("return", response.status === 201 ? new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(data.id, data.email, data.name, data.body, data.postId) : null);

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function addComment(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
}();
/**
 * Deletes a comment for the given id
 *
 * @param {number} id The id of the Comment to delete
 * @returns {Promise<Comment | null>}
 */

var deleteComment = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(id) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/comments/".concat(id), {
              method: "DELETE"
            });

          case 2:
            response = _context2.sent;
            data = response.json(); // Analizzo il codice della risposta e restituisco un valore di conseguenza

            return _context2.abrupt("return", response.status === 200 ? new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(data.id, data.email, data.name, data.body, data.postId) : null);

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function deleteComment(_x5) {
    return _ref2.apply(this, arguments);
  };
}();
var getComment = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(id) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/comments/".concat(id));

          case 2:
            response = _context3.sent;
            _context3.next = 5;
            return response.json();

          case 5:
            data = _context3.sent;
            return _context3.abrupt("return", response.status === 200 ? new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(data.id, data.email, data.name, data.body, data.postId) : null);

          case 7:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function getComment(_x6) {
    return _ref3.apply(this, arguments);
  };
}();
/**
 * Fetch the comments for the given post id
 *
 * @param {number} postId The post Id
 * @returns {Promise<Comment[]>}
 */

var getComments = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(postId) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/posts/".concat(postId, "/comments"));

          case 2:
            response = _context4.sent;
            _context4.next = 5;
            return response.json();

          case 5:
            data = _context4.sent;
            return _context4.abrupt("return", data.map(function (c) {
              return new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(c.id, c.email, c.name, c.body, c.postId);
            }));

          case 7:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function getComments(_x7) {
    return _ref4.apply(this, arguments);
  };
}();
var putComment = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(id, email, name, body, postId) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/comments/".concat(id), {
              body: JSON.stringify({
                body: body,
                email: email,
                id: id,
                name: name,
                postId: postId
              }),
              headers: {
                "Content-Type": "application/json; charset=utf-8"
              },
              method: "PUT"
            });

          case 2:
            response = _context5.sent;
            _context5.next = 5;
            return response.json();

          case 5:
            data = _context5.sent;
            return _context5.abrupt("return", new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(data.id, data.email, data.name, data.body, data.postId));

          case 7:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function putComment(_x8, _x9, _x10, _x11, _x12) {
    return _ref5.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./src/functions/post.js":
/*!*******************************!*\
  !*** ./src/functions/post.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getPost": () => (/* binding */ getPost)
/* harmony export */ });
/* harmony import */ var _models_post__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../models/post */ "./src/models/post.js");
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


/**
 * Read a post for the given id
 *
 * @param {number} id The post id
 * @returns {Promise<Post>}
 */

var getPost = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(id) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/posts/".concat(id));

          case 2:
            response = _context.sent;
            _context.next = 5;
            return response.json();

          case 5:
            data = _context.sent;
            return _context.abrupt("return", new _models_post__WEBPACK_IMPORTED_MODULE_0__.Post(data.id, data.title, data.body, data.userId));

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function getPost(_x) {
    return _ref.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./src/models/comment.js":
/*!*******************************!*\
  !*** ./src/models/comment.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Comment": () => (/* binding */ Comment)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Comment = function Comment(id, email, name, body, postId) {
  _classCallCheck(this, Comment);

  this.body = body;
  this.email = email;
  this.id = id;
  this.name = name;
  this.postId = postId;
};

/***/ }),

/***/ "./src/models/post.js":
/*!****************************!*\
  !*** ./src/models/post.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Post": () => (/* binding */ Post)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Post = function Post(id, title, body, userId) {
  _classCallCheck(this, Post);

  this.body = body;
  this.id = id;
  this.title = title;
  this.userId = userId;
};

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!*********************!*\
  !*** ./src/post.js ***!
  \*********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _functions_comments__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./functions/comments */ "./src/functions/comments.js");
/* harmony import */ var _functions_post__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./functions/post */ "./src/functions/post.js");
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var addCommentForm = document.querySelector('form[name="add-comment"]'); // Comments 1. Individuo gli elementi nel codice HTML

var commentsSection = document.querySelector("#comments"); // Post 1. Individuo gli elementi nel codice HTML

var postContent = document.querySelector(".post > p");
var postTitle = document.querySelector(".post > h1");

var appendComment = function appendComment(c) {
  var row = document.createElement("div"); // Esempio di set / lettura di un data attribute
  // La nomenclatura dash case di HTML viene convertita in JavaScript in camelCase
  //
  // row.setAttribute('data-tuo-attributo');
  // row.dataset.tuoAttributo

  row.className = "row";
  row.innerHTML = "\n      <div class=\"col-lg-12\">\n          <h3>".concat(c.name, "</h3>\n          <p>").concat(c.body, "</p>\n          <p>Scritto da: ").concat(c.email, "</p>\n      </div>\n  "); // <span class="delete-comment" data-id="${c.id}">X</span>

  var span = document.createElement("span");
  span.className = "delete-comment";
  span.setAttribute("data-id", c.id);
  span.innerText = "X";
  span.addEventListener("click", /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(e) {
      var deletedComment;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              // Tutti i data attributes vengono agglomerati in una chiave "dataset" presente sull'elemento HTML
              // Se creiamo un data-attribute
              console.log(e.target.dataset.id); // L'id del commento posso ricavarmelo in due modi:
              // 1. Sfruttando il data attribute "id" => e.target.dataset.id
              // 2. Tenendo in considerazione che tutte le funzioni in JS sono closure, quindi anche nella funzione
              //    di questo ascoltatore posso accedere al parametro "c" che contiene l'oggetto Comment

              _context.next = 3;
              return (0,_functions_comments__WEBPACK_IMPORTED_MODULE_0__.deleteComment)(e.target.dataset.id);

            case 3:
              deletedComment = _context.sent;
              console.log(deletedComment); // Rimuovo la riga corrispondente al commento rimosso dal backend
              // Per farlo, ho nuovamente due modi:
              // 1. Individuo la riga del commento cercando lo span con un particolare data-id (che coincide
              //    con l'id del commento da eliminare)
              // 2. Sfrutto le closure: se devo rimuovere la riga, invoce il metodo remove() dalla costante row
              // Se non ottengo un oggetto (codice diverso da 200) la risorsa non è stata rimossa dal backend
              // quindi non la rimuovo dall'interfaccia

              if (deletedComment) {
                _context.next = 7;
                break;
              }

              return _context.abrupt("return");

            case 7:
              row.remove();

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }());
  row.firstElementChild.firstElementChild.append(span);
  commentsSection.append(row);
};

var readComments = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    var comments;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return (0,_functions_comments__WEBPACK_IMPORTED_MODULE_0__.getComments)(1);

          case 2:
            comments = _context2.sent;
            comments.forEach(function (c) {
              return appendComment(c);
            });

          case 4:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function readComments() {
    return _ref2.apply(this, arguments);
  };
}();

var readPost = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
    var post;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return (0,_functions_post__WEBPACK_IMPORTED_MODULE_1__.getPost)(1);

          case 2:
            post = _context3.sent;
            postContent.innerHTML = post.body;
            postTitle.innerHTML = post.title;

          case 5:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function readPost() {
    return _ref3.apply(this, arguments);
  };
}(); // Eventualmente, al posto di usare una funzione async per caricare
// i dati: getPost(1).then(...)


readPost();
readComments();
addCommentForm.addEventListener("submit", /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(e) {
    var content, email, name, c;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            e.preventDefault(); // Raccogliere i dati da inviare al backend

            content = document.querySelector('input[name="content"]').value;
            email = document.querySelector('input[name="email"]').value;
            name = document.querySelector('input[name="name"]').value; // Aggiungi il commento e preleva la risposta dal backend

            _context4.next = 6;
            return (0,_functions_comments__WEBPACK_IMPORTED_MODULE_0__.addComment)(email, name, content, 1);

          case 6:
            c = _context4.sent;
            appendComment(c);

          case 8:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x2) {
    return _ref4.apply(this, arguments);
  };
}());
})();

/******/ })()
;
//# sourceMappingURL=post.bundle.js.map