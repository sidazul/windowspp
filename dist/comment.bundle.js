/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/functions/comments.js":
/*!***********************************!*\
  !*** ./src/functions/comments.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "addComment": () => (/* binding */ addComment),
/* harmony export */   "deleteComment": () => (/* binding */ deleteComment),
/* harmony export */   "getComment": () => (/* binding */ getComment),
/* harmony export */   "getComments": () => (/* binding */ getComments),
/* harmony export */   "putComment": () => (/* binding */ putComment)
/* harmony export */ });
/* harmony import */ var _models_comment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../models/comment */ "./src/models/comment.js");
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


/**
 * Create a comment accordingly to the given params
 *
 * @param {string} email User's email
 * @param {string} name User's name
 * @param {string} body Content of the comment
 * @param {number} postId Id of the post
 * @returns {Promise<Comment | null>}
 */

var addComment = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(email, name, body, postId) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/posts/".concat(postId, "/comments"), {
              body: JSON.stringify({
                // È una contrazione per body: body, email: email etc etc
                body: body,
                email: email,
                name: name,
                postId: postId
              }),
              headers: {
                "Content-Type": "application/json; charset=utf-8"
              },
              method: "POST"
            });

          case 2:
            response = _context.sent;
            _context.next = 5;
            return response.json();

          case 5:
            data = _context.sent;
            return _context.abrupt("return", response.status === 201 ? new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(data.id, data.email, data.name, data.body, data.postId) : null);

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function addComment(_x, _x2, _x3, _x4) {
    return _ref.apply(this, arguments);
  };
}();
/**
 * Deletes a comment for the given id
 *
 * @param {number} id The id of the Comment to delete
 * @returns {Promise<Comment | null>}
 */

var deleteComment = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(id) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/comments/".concat(id), {
              method: "DELETE"
            });

          case 2:
            response = _context2.sent;
            data = response.json(); // Analizzo il codice della risposta e restituisco un valore di conseguenza

            return _context2.abrupt("return", response.status === 200 ? new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(data.id, data.email, data.name, data.body, data.postId) : null);

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function deleteComment(_x5) {
    return _ref2.apply(this, arguments);
  };
}();
var getComment = /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(id) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/comments/".concat(id));

          case 2:
            response = _context3.sent;
            _context3.next = 5;
            return response.json();

          case 5:
            data = _context3.sent;
            return _context3.abrupt("return", response.status === 200 ? new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(data.id, data.email, data.name, data.body, data.postId) : null);

          case 7:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function getComment(_x6) {
    return _ref3.apply(this, arguments);
  };
}();
/**
 * Fetch the comments for the given post id
 *
 * @param {number} postId The post Id
 * @returns {Promise<Comment[]>}
 */

var getComments = /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(postId) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/posts/".concat(postId, "/comments"));

          case 2:
            response = _context4.sent;
            _context4.next = 5;
            return response.json();

          case 5:
            data = _context4.sent;
            return _context4.abrupt("return", data.map(function (c) {
              return new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(c.id, c.email, c.name, c.body, c.postId);
            }));

          case 7:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function getComments(_x7) {
    return _ref4.apply(this, arguments);
  };
}();
var putComment = /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(id, email, name, body, postId) {
    var response, data;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return fetch("http://jsonplaceholder.typicode.com/comments/".concat(id), {
              body: JSON.stringify({
                body: body,
                email: email,
                id: id,
                name: name,
                postId: postId
              }),
              headers: {
                "Content-Type": "application/json; charset=utf-8"
              },
              method: "PUT"
            });

          case 2:
            response = _context5.sent;
            _context5.next = 5;
            return response.json();

          case 5:
            data = _context5.sent;
            return _context5.abrupt("return", new _models_comment__WEBPACK_IMPORTED_MODULE_0__.Comment(data.id, data.email, data.name, data.body, data.postId));

          case 7:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function putComment(_x8, _x9, _x10, _x11, _x12) {
    return _ref5.apply(this, arguments);
  };
}();

/***/ }),

/***/ "./src/models/comment.js":
/*!*******************************!*\
  !*** ./src/models/comment.js ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Comment": () => (/* binding */ Comment)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Comment = function Comment(id, email, name, body, postId) {
  _classCallCheck(this, Comment);

  this.body = body;
  this.email = email;
  this.id = id;
  this.name = name;
  this.postId = postId;
};

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!************************!*\
  !*** ./src/comment.js ***!
  \************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _functions_comments__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./functions/comments */ "./src/functions/comments.js");
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


var form = document.querySelector('form[name="edit-comment"]'); // Form's input fields

var content = document.querySelector('input[name="content"]');
var email = document.querySelector('input[name="email"]');
var id = document.querySelector('input[name="id"]');
var name = document.querySelector('input[name="name"]');
var postId = document.querySelector('input[name="post-id"]');

var load = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var comment;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return (0,_functions_comments__WEBPACK_IMPORTED_MODULE_0__.getComment)(3);

          case 2:
            comment = _context.sent;
            // Imposto i campi in input del form con i valori che ho ricevuto dalla chiamata
            content.value = comment.body;
            email.value = comment.email;
            id.value = comment.id;
            name.value = comment.name;
            postId.value = comment.postId;

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function load() {
    return _ref.apply(this, arguments);
  };
}();

load();
form.addEventListener("submit", /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(e) {
    var comment;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            e.preventDefault();
            _context2.next = 3;
            return (0,_functions_comments__WEBPACK_IMPORTED_MODULE_0__.putComment)(id.value, email.value, name.value, content.value, postId.value);

          case 3:
            comment = _context2.sent;
            console.log(comment);

          case 5:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x) {
    return _ref2.apply(this, arguments);
  };
}());
})();

/******/ })()
;
//# sourceMappingURL=comment.bundle.js.map