/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./environments/develop.js":
/*!*********************************!*\
  !*** ./environments/develop.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "config": () => (/* binding */ config)
/* harmony export */ });
var config = {
  weather: {
    apiKey: "9960bded1cd4581570a95916efa4ffc5"
  }
};

/***/ }),

/***/ "./src/classes/weather.js":
/*!********************************!*\
  !*** ./src/classes/weather.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Weather": () => (/* binding */ Weather)
/* harmony export */ });
/* harmony import */ var _environments_develop__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../environments/develop */ "./environments/develop.js");
/* harmony import */ var _models_city__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../models/city */ "./src/models/city.js");
/* harmony import */ var _models_forecast__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/forecast */ "./src/models/forecast.js");
/* harmony import */ var _models_temperature__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../models/temperature */ "./src/models/temperature.js");
/* harmony import */ var _models_wind__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../models/wind */ "./src/models/wind.js");
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

 // Models





var Weather = /*#__PURE__*/function () {
  function Weather(city, state, country) {
    _classCallCheck(this, Weather);

    this.city = city;
    this.country = country;
    this.state = state;
  } // update() {
  //     fetch(`http://api.openweathermap.org/data/2.5/weather?q=${this.city},${this.state},${this.country}&appid=${config.weather.apiKey}`)
  //         .then(response => response.json())
  //         .then(data => {
  //             // Trasformo i dati in un Model "Forecast"
  //         });
  // }

  /**
   * Fetch the weather data from the OpenWeatherMap API
   *
   * @param {string} lang Language of the response text
   * @returns {Promise}
   */


  _createClass(Weather, [{
    key: "update",
    value: function () {
      var _update = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
        var lang,
            response,
            data,
            _args = arguments;
        return regeneratorRuntime.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                lang = _args.length > 0 && _args[0] !== undefined ? _args[0] : "it";
                _context.next = 3;
                return fetch("http://api.openweathermap.org/data/2.5/weather?q=".concat(this.city, ",").concat(this.state, ",").concat(this.country, "&appid=").concat(_environments_develop__WEBPACK_IMPORTED_MODULE_0__.config.weather.apiKey, "&units=metric&lang=").concat(lang));

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.json();

              case 6:
                data = _context.sent;
                return _context.abrupt("return", new _models_forecast__WEBPACK_IMPORTED_MODULE_2__.Forecast(new _models_city__WEBPACK_IMPORTED_MODULE_1__.City(data.name, data.sys.country, data.coord.lat, data.coord.lon, data.timezone, data.sunrise, data.sunset), data.weather[0].description, new _models_temperature__WEBPACK_IMPORTED_MODULE_3__.Temperature(data.main.temp, data.main.temp_min, data.main.temp_max, data.main.feels_like), new _models_wind__WEBPACK_IMPORTED_MODULE_4__.Wind(data.wind.speed, data.wind.deg, data.wind.gust), data.main.pressure, data.main.humidity));

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function update() {
        return _update.apply(this, arguments);
      }

      return update;
    }()
  }]);

  return Weather;
}();
var w = new Weather("rome", "IT-62", "IT");
w.update();

/***/ }),

/***/ "./src/models/city.js":
/*!****************************!*\
  !*** ./src/models/city.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "City": () => (/* binding */ City)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var City =
/**
 * @param {string} name City name
 * @param {string} country Country Code (i.e. IT)
 * @param {number} lat Latitude
 * @param {number} lon Longitude
 * @param {number} tz Timezone: minutes from the GMT+0
 * @param {number} sunrise Sunrise timestamp
 * @param {number} sunset Sunset timestamp
 */
function City(name, country, lat, lon, tz) {
  var sunrise = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : 0;
  var sunset = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : 0;

  _classCallCheck(this, City);

  this.country = country;
  this.lat = lat;
  this.lon = lon;
  this.name = name;
  this.sunrise = sunrise;
  this.sunset = sunset;
  this.tz = tz;
};

/***/ }),

/***/ "./src/models/forecast.js":
/*!********************************!*\
  !*** ./src/models/forecast.js ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Forecast": () => (/* binding */ Forecast)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Forecast = /*#__PURE__*/function () {
  /**
   * @param {City} city
   * @param {string} description
   * @param {Temperature} temperature
   * @param {Wind} wind
   * @param {number} pressure
   * @param {number} humidity
   */
  function Forecast(city, description, temperature, wind, pressure, humidity) {
    _classCallCheck(this, Forecast);

    this.city = city;
    this.description = description;
    this.humidity = humidity;
    this.pressure = pressure;
    this.temperature = temperature;
    this.wind = wind;
  } // Un getter (un metodo che usa come parola chiave "get") crea una proprietà fittizia all'interno dell'istanza
  // il cui valore è ciò che viene restituito dal metodo
  // i.e. this.iconUrl <- NO parentesi tonde (si comporta come una proprietà)


  _createClass(Forecast, [{
    key: "iconUrl",
    get: function get() {
      return "http://openweathermap.org/img/wn/".concat(this.icon, "@2x.png");
    } // Questo è un metodo qualsiasi, e per leggerne il valore restituito devo invocarlo
    // i.e. this.getIconUrl() <- SI parentesi tonde (è un metodo, deve essere invocato)

  }, {
    key: "getIconURL",
    value: function getIconURL() {
      return "http://openweathermap.org/img/wn/".concat(this.icon, "@2x.png");
    }
  }]);

  return Forecast;
}();

/***/ }),

/***/ "./src/models/temperature.js":
/*!***********************************!*\
  !*** ./src/models/temperature.js ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Temperature": () => (/* binding */ Temperature)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Temperature = function Temperature(current, min, max, feelsLike) {
  _classCallCheck(this, Temperature);

  this.current = current;
  this.feelsLike = feelsLike;
  this.max = max;
  this.min = min;
};

/***/ }),

/***/ "./src/models/wind.js":
/*!****************************!*\
  !*** ./src/models/wind.js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Wind": () => (/* binding */ Wind)
/* harmony export */ });
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Wind = function Wind(speed, degrees, gust) {
  _classCallCheck(this, Wind);

  this.degrees = degrees;
  this.gust = gust;
  this.speed = speed;
};

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!**********************!*\
  !*** ./src/meteo.js ***!
  \**********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _classes_weather__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/weather */ "./src/classes/weather.js");
function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


var city = document.getElementById("city");
var forecast = document.getElementById("forecast");
var icon = document.getElementById("icon");
var temp = document.getElementById("temp"); // Con Promise chaining:
// weather.update().then(data => console.log(data));
// Per poter utilizzare async / await dobbiamo necessariamente trovarci dentro una funzione
// che sia dichiarata come async

var load = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    var weather, data;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            weather = new _classes_weather__WEBPACK_IMPORTED_MODULE_0__.Weather("Rome", "IT-62", "IT");
            _context.next = 3;
            return weather.update();

          case 3:
            data = _context.sent;
            city.innerText = "".concat(data.city.name, " - ").concat(data.description);
            forecast.innerHTML = "\n    ".concat(data.temperature.current, "\xB0C (").concat(data.temperature.feelsLike, " percepiti)\n    Min: ").concat(data.temperature.min, " - Max: ").concat(data.temperature.max, "\n  ");
            icon.src = data.iconUrl;
            console.log(data); // Utilizzando il getter di una classe, accedo al valore come se leggessi il contenuto di una proprietà:
            // i.e. leggo il valore del getter iconUrl
            // forecast.iconUrl
            // Invocando un metodo, uso invece le parentesi tonde:
            // i.e. forecast.getIconUrl()

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function load() {
    return _ref.apply(this, arguments);
  };
}();

load();
})();

/******/ })()
;
//# sourceMappingURL=meteo.bundle.js.map